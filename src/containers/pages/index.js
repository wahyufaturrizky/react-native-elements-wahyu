import Login from './Login';
import Home from './Home';
import StudyGuide from './StudyGuide';
import Message from './Message';
import MessageWhatsApp from './MessageWhatsApp';

export {
    Login,
    Home,
    StudyGuide,
    Message,
    MessageWhatsApp
}