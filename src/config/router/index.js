import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Login, Home, StudyGuide, Message, MessageWhatsApp } from '../../containers/pages';

const LoginStack = createStackNavigator(
	{
		Login,
	},
	{
		headerMode: 'none',
		initialRouteParams: 'Login'
	}
)

const HomeStack = createStackNavigator(
	{
		Home,
		StudyGuide
	},
	{
		headerMode: 'none',
		initialRouteParams: 'Home'
	}
)

const MessageStack = createStackNavigator(
	{
		Message,
		MessageWhatsApp
	},
	{
		headerMode: 'none',
		initialRouteParams: 'Message'
	}
)

const Router = createSwitchNavigator(
	{
		LoginStack,
		HomeStack,
		MessageStack
	},
	{
		headerMode: 'none',
		initialRouteParams: 'HomeStack'
	}
);
  
export default createAppContainer(Router);